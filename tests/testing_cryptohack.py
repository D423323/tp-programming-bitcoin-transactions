from btctoy.crypto import (
    EllipcCurvePoint,
)

p = EllipcCurvePoint(x=493, y=5564, a=497, b=1768)
q = EllipcCurvePoint(x=1539, y=4742, a=497, b=1768)
r = EllipcCurvePoint(x=4403, y=5202, a=497, b=1768)


print(p + q + r)